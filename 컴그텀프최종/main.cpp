//space바       -> 게임 시작
//키보드 화살표 -> 원하는 방향으로 이동
//숫자 1        -> 1인칭
//숫자 2        -> 3인칭
//숫자 3,4      -> 디퓨즈 항 변경
//숫자 5,6      -> 스페큘러 항 변경
//숫자 7,8      -> 엠비언트 항 변경
//r, t          -> 조명 키고 끄기
//i, I          -> 확대 축소
//wasd          -> 맵 평행 이동
//x, X          -> x축 회전
//y, Y          -> y축 회전
//z, Z          -> z축 회전
#include "main.h"

double timeCount;

void main(int argc, char * argv[])
{
	//초기화 함수
	srand(unsigned(time(NULL)));
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(800, 600);
	glutCreateWindow("컴그실습15");
	initTextures();
	init_sound();
	ifstream in("map.txt");

	for (int i = 0; i < 20; ++i)
	{
		for (int j = 0; j < 20; ++j)
		{
			in >> temp[i][j];
		}
	}

	ifstream in2("map2.txt");

	for (int i = 0; i < 20; i++)
	{
		for (int j = 0; j < 20; j++)
		{
			in2 >> temp2[i][j];
		}
	}

	ifstream in3("highscore.txt");

	in3 >> displayHigh;

	glFrontFace(GL_CCW);
	glEnable(GL_LIGHTING);
	glutSpecialFunc(SpecialKeyboard);
	glutKeyboardFunc(keyboard);
	glutDisplayFunc(drawScene);
	glutReshapeFunc(Reshape);
	glutTimerFunc(100, TimerFunc, 1);

	glutMainLoop();
}
void init_sound()
{
	if (state == 0)
		PlaySound(TEXT("pacman_beginning.wav"), NULL, SND_FILENAME | SND_ASYNC);
	else if (state == 1 || state == 2)
	{
		PlaySound(TEXT("mainbgm.wav"), NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);
	}
	else if (state == 4 && gameover_sound == true)
	{
		PlaySound(TEXT("pacman_death.wav"), NULL, SND_FILENAME | SND_ASYNC);
		gameover_sound = false;
	}
}
void init_all()
{

	for (int i = 0; i < 20; i++)
	{
		for (int j = 0; j < 20; j++)
		{
			if (temp[i][j] == 1)
			{
				wall[i][j].x = -475 + 50 * j;
				wall[i][j].y = 50;
				wall[i][j].z = -475 + 50 * i;
				wall[i][j].color_r = (rand() % 9 + 1) * 0.1;
				wall[i][j].color_g = (rand() % 9 + 1) * 0.1;
				wall[i][j].color_b = (rand() % 9 + 1)* 0.1;
			}
			else if (temp[i][j] == 0)
			{
				seed[i][j].x = -475 + 50 * j;
				seed[i][j].y = 50;
				seed[i][j].z = -475 + 50 * i;
				seed[i][j].collide_variable = true;

			}
		}
	}
	enemy[0].x = -425, enemy[0].y = 50, enemy[0].z = -425;
	enemy[1].x = 425, enemy[1].y = 50, enemy[1].z = -425;
	enemy[2].x = -375, enemy[2].y = 50, enemy[2].z = 375;
	enemy[3].x = 425, enemy[3].y = 50, enemy[3].z = 425;
	for (int i = 0; i < 4; i++)
	{
		enemy[i].dir = 0;
		enemy[i].speed = 10;
		enemy[i].degree = 0.0;

	}

	my_x = -175.0, my_y = 50, my_z = 325.0;
	my_dir = 2;
	my_degree = 0.0;
	my_speed = 10;
	eat_count = 0;

	if (state == 2)
	{
		for (int i = 0; i < 20; i++)
		{
			for (int j = 0; j < 20; j++)
			{
				if (temp2[i][j] == 1)
				{
					wall2[i][j].x = -475 + 50 * j;
					wall2[i][j].y = 50;
					wall2[i][j].z = -475 + 50 * i;
					wall2[i][j].color_r = (rand() % 9 + 1) * 0.1;
					wall2[i][j].color_g = (rand() % 9 + 1) * 0.1;
					wall2[i][j].color_b = (rand() % 9 + 1)* 0.1;
				}
				else if (temp2[i][j] == 0)
				{
					seed2[i][j].x = -475 + 50 * j;
					seed2[i][j].y = 50;
					seed2[i][j].z = -475 + 50 * i;
					seed2[i][j].collide_variable = true;

				}
			}
		}

		my_x = -175.0, my_y = 50, my_z = 175.0;
		my_dir = 2;
		my_degree = 0.0;
		my_speed = 10;
		eat_count2 = 0;
		enemy2[0].x = -425, enemy2[0].y = 50, enemy2[0].z = -425;
		enemy2[1].x = -125, enemy2[1].y = 50, enemy2[1].z = -425;
		enemy2[2].x = -25, enemy2[2].y = 50, enemy2[2].z = -425;
		enemy2[3].x = 175, enemy2[3].y = 50, enemy2[3].z = -425;
		enemy2[4].x = 425, enemy2[4].y = 50, enemy2[4].z = -425;
		enemy2[5].x = -425, enemy2[5].y = 50, enemy2[5].z = 425;
		enemy2[6].x = 425, enemy2[6].y = 50, enemy2[6].z = 425;

		for (int i = 0; i < 4; i++)
		{
			enemy2[i].dir = 0;
			enemy2[i].speed = 10;
			enemy2[i].degree = 0.0;

		}
	}

	view_trans = true;



} //if문 수정
void Ai()
{

	if (state == 1)
	{
		for (int i = 0; i < 4; i++)
		{
			glPushMatrix();
			{
				glColor3f(0.5, 0.5, 0.5);
				glTranslatef(enemy[i].x, enemy[i].y, enemy[i].z);
				glRotatef(enemy[i].degree, 0.0, 1.0, 0.0);
				glutSolidSphere(20, 20, 20);


			}
			glPopMatrix();
		}
	}

	else if (state == 2)
	{
		for (int i = 0; i < 7; i++)
		{
			glPushMatrix();
			{
				glColor3f(0.5, 0.5, 0.5);
				glTranslatef(enemy2[i].x, enemy2[i].y, enemy2[i].z);
				glRotatef(enemy2[i].degree, 0.0, 1.0, 0.0);
				glutSolidSphere(20, 20, 20);
			}
			glPopMatrix();
		}
	}


}
void Floor() // 바닥과 벽 만드는 함수
{

	glPushMatrix();//바닥
	{
		glPushMatrix(); //벽과 바닥
		{
			glColor3f(0, 0, 0);//
			glBegin(GL_QUADS);
			glTexCoord2f(0, 0);
			glVertex3f(-500, 0, -500);
			glTexCoord2f(0, 1);
			glVertex3f(-500, 0, 500);
			glTexCoord2f(1, 1);
			glVertex3f(500, 0, 500);
			glTexCoord2f(1, 0);
			glVertex3f(500, 0, -500);
			glEnd();

		}
		glPopMatrix();

		if (state == 1)
		{
			for (int i = 0; i < 20; i++)
			{
				for (int j = 0; j < 20; j++)
				{
					glPushMatrix();
					{
						if (wall[i][j].x != 0 && wall[i][j].z != 0 && wall[i][j].y != 0)
						{
							glColor3f(wall[i][j].color_r, wall[i][j].color_g, wall[i][j].color_b);
							glTranslatef(wall[i][j].x, wall[i][j].y, wall[i][j].z);


							glutSolidCube(50);

						}
					}
					glPopMatrix();
				}
			}
		}
		else if (state == 2)
		{
			for (int i = 0; i < 20; i++)
			{
				for (int j = 0; j < 20; j++)
				{
					glPushMatrix();
					{
						if (wall2[i][j].x != 0 && wall2[i][j].z != 0 && wall2[i][j].y != 0)
						{
							glColor3f(wall2[i][j].color_r, wall2[i][j].color_g, wall2[i][j].color_b);
							glTranslatef(wall2[i][j].x, wall2[i][j].y, wall2[i][j].z);
							glutSolidCube(50);
						}
					}
					glPopMatrix();
				}
			}
		}

	}
	glPopMatrix();


}
void Player()// 플레이어 
{

	glPushMatrix();
	{
		glColor3f(1.0, 0.0, 1.0);
		glTranslatef(my_x, my_y, my_z);
		glRotatef(my_degree, 0.0, 1.0, 0.0);
		glutSolidSphere(20, 20, 20);
	}
	glPopMatrix();

}
void ITEM()
{
	if (state == 1)
	{
		for (int i = 0; i < 20; i++)
		{
			for (int j = 0; j < 20; j++)
			{
				if (seed[i][j].collide_variable)
				{
					glPushMatrix();
					{
						glColor3f(1.0, 1.0, 0.0);
						glTranslatef(seed[i][j].x, seed[i][j].y, seed[i][j].z);
						glutSolidSphere(5, 20, 20);
					}
					glPopMatrix();
				}
			}
		}
	}
	else if (state == 2)
	{
		for (int i = 0; i < 20; i++)
		{
			for (int j = 0; j < 20; j++)
			{
				if (seed2[i][j].collide_variable)
				{
					glPushMatrix();
					{
						glColor3f(1.0, 1.0, 0.0);
						glTranslatef(seed2[i][j].x, seed2[i][j].y, seed2[i][j].z);
						glutSolidSphere(5, 20, 20);
					}
					glPopMatrix();
				}
			}
		}
	}
}
void SpecialKeyboard(int key, int x, int y)
{
	if (key == GLUT_KEY_UP)
	{
		my_dir = 1;
	}
	if (key == GLUT_KEY_DOWN)
	{
		my_dir = 0;
	}
	if (key == GLUT_KEY_LEFT)
	{
		my_dir = 3;
	}
	if (key == GLUT_KEY_RIGHT)
	{
		my_dir = 2;
	}
}
void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case '+':
		isGameOver = false;
		nowScore = 0;
		state = 0;
		init_sound();
		gameover_sound = true;
		break;

	case ' ':
		if (state != 0) { break; }
		init_all();
		state = 1;
		init_sound();
		break;
	case 'x':
		if (state == 0) { break; } // 인트로화면이면 키가 먹지 않게
		rotate_x += 10;
		break;
	case 'y':
		if (state == 0) { break; }
		rotate_y += 10;
		break;
	case 'z':
		if (state == 0) { break; }
		rotate_z += 10;
		break;
	case 'X':
		if (state == 0) { break; }
		rotate_x -= 10;
		break;
	case 'Y':
		if (state == 0) { break; }
		rotate_y -= 10;
		break;
	case 'Z':
		if (state == 0) { break; }
		rotate_z -= 10;
		break;
	case 'w':
		if (state == 0) { break; }
		eye_y += 5;
		break;
	case 's':
		if (state == 0) { break; }
		eye_y -= 5;
		break;
	case 'a':
		if (state == 0) { break; }
		eye_x -= 5;
		break;
	case 'd':
		if (state == 0) { break; }
		eye_x += 5;
		break;

	case 'i':
		if (state == 0) { break; }
		camera_zoom_in += 0.5;
		break;
	case 'I':
		if (state == 0) { break; }
		camera_zoom_in -= 0.5;
		break;

	case '1':
		if (state == 0) { break; }
		view_trans = false;
		break;
	case '2':
		if (state == 0) { break; }
		view_trans = true;
		break;

	case 'r':
		if (state == 0) { break; }

		if (light_1_state)
			light_1_state = false;
		else
			light_1_state = true;

		break;
	case 't':
		if (state == 0) { break; }

		if (light_2_state)
			light_2_state = false;
		else
			light_2_state = true;
		break;
	case '3':
		if (state == 0) { break; }

		if (diffuse_quantity_select == true)
		{
			diffuse_quantity -= 0.1;
			if (diffuse_quantity <= 0)
				diffuse_quantity_select = false;
		}
		break;
	case '4':
		if (state == 0) { break; }

		if (diffuse_quantity_select == false)
		{
			diffuse_quantity += 0.1;
			if (diffuse_quantity >= 1)
				diffuse_quantity_select = true;
		}
		break;
	case '5':
		if (state == 0) { break; }

		if (specular_quantity_select == true)
		{
			specular_quantity -= 0.1;
			if (specular_quantity <= 0)
				specular_quantity_select = false;
		}
		break;
	case '6':
		if (state == 0) { break; }

		if (specular_quantity_select == false)
		{
			specular_quantity += 0.1;
			if (specular_quantity >= 1)
				specular_quantity_select = true;
		}
		break;
	case '7':
		if (state == 0) { break; }

		if (ambient_quantity_select == true)
		{
			ambient_quantity -= 0.1;
			if (ambient_quantity <= 0)
				ambient_quantity_select = false;
		}
		break;
	case '8':
		if (state == 0) { break; }

		if (ambient_quantity_select == false)
		{
			ambient_quantity += 0.1;
			if (ambient_quantity >= 1)
				ambient_quantity_select = true;
		}
		break;
	default:
		break;


	}
	for (int i = 0; i < 3; ++i)
	{
		DiffuseLight[i] = 1.0f * diffuse_quantity;
		SpecularLight[i] = 1.0f * specular_quantity;
		AmbientLight[i] = 1.0f * ambient_quantity;
	}
	glutPostRedisplay();
}
void viewing_trans(void) // 시점 변환을 위한 함수
{
	glRotatef(30, 1.0, 0.0, 0.0);
	if (my_dir == 0) // 아래
	{

		gluLookAt(my_x, my_y + 200, my_z - 150, my_x, my_y + 20, my_z + 300, 0, 1, 0); // 1인칭 시점 만들기
	}
	else if (my_dir == 1) // 위
	{

		gluLookAt(my_x, my_y + 200, my_z + 150, my_x, my_y + 20, my_z - 300, 0, 1, 0); // 1인칭 시점 만들기
	}
	else if (my_dir == 2) // 오른쪽
	{

		gluLookAt(my_x - 200, my_y + 200, my_z, my_x + 300, my_y + 20, my_z, 0, 1, 0); // 1인칭 시점 만들기
	}
	else if (my_dir == 3) // 왼쪽
	{

		gluLookAt(my_x + 200, my_y + 200, my_z, my_x - 300, my_y + 20, my_z, 0, 1, 0); // 1인칭 시점 만들기
	}

}
void Ui()
{
	glPushMatrix();
	{
		glPushMatrix(); // 타이머
		{
			char string[] = "TIME : ";
			glColor3f(1, 0, 0);
			print(-570, 810, 0, string);

			glColor3f(1, 0, 0);
			print(-360, 810, 0, itoa(600 - (timeCount / 10), buffer, 10));

		}
		glPopMatrix();


		glPushMatrix(); // 라이프
		{
			char string[] = "Life : ";
			glColor3f(1, 0, 0);
			print(-250, 810, 0, string);

			glColor3f(1, 0, 0);
			print(-100, 810, 0, itoa(my_life, buffer, 10));

		}
		glPopMatrix();


		glPushMatrix(); // 현재 스코어
		{
			char string[] = "Score : ";
			glColor3f(1, 0, 0);
			print(0, 810, 0, string);

			glColor3f(1, 0, 0);
			print(200, 810, 0, itoa(nowScore * 10, buffer, 10));

		}
		glPopMatrix();

		glPushMatrix(); // 하이 스코어
		{
			char string[] = "HighScore : ";
			glColor3f(1, 0, 0);
			print(330, 810, 0, string);

			glColor3f(1, 0, 0);
			print(680, 810, 0, itoa(displayHigh, buffer, 10));




		}
		glPopMatrix();

	}
	glPopMatrix();


}
GLvoid drawScene(GLvoid)
{
	glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();
	{
		glLightfv(GL_LIGHT1, GL_AMBIENT, AmbientLight);
		glLightfv(GL_LIGHT1, GL_DIFFUSE, DiffuseLight);
		glLightfv(GL_LIGHT1, GL_SPECULAR, SpecularLight);
		glLightfv(GL_LIGHT1, GL_POSITION, lightPos1);

		if (light_1_state)
			glEnable(GL_LIGHT1);
		else
			glDisable(GL_LIGHT1);

		glLightfv(GL_LIGHT2, GL_AMBIENT, AmbientLight);
		glLightfv(GL_LIGHT2, GL_DIFFUSE, DiffuseLight);
		glLightfv(GL_LIGHT2, GL_SPECULAR, SpecularLight);
		glLightfv(GL_LIGHT2, GL_POSITION, lightPos2);

		if (light_2_state)
			glEnable(GL_LIGHT2);
		else
			glDisable(GL_LIGHT2);

		glMateriali(GL_FRONT, GL_SHININESS, 128);
		glEnable(GL_COLOR_MATERIAL);
		glColorMaterial(GL_FRONT_AND_BACK, GL_SPECULAR);
		glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);

		if (state == 0) //인트로 라면
		{
			timeCount = 0;
			GameIntro();
		}
		else if (state == 4)
		{
			if (nowScore * 10 >= displayHigh)
			{
				Write_HighScore = 1;
			}
			if (Write_HighScore == 1)
			{
				FormerHigh = displayHigh;
				LastScore = FormerHigh;
				displayHigh = nowScore * 10;;
			}
			else
			{
				LastScore = nowScore * 10;
			}
			GameOver();
		}
		else if (state == 1) // 인트로가 아니라면 (stage 1)
		{
			glPushMatrix(); //ui
			{
				Ui();
			}
			glPopMatrix();
			if (view_trans) // 3인칭 시점
			{
				glTranslatef(eye_x, eye_y, eye_z);
				glRotatef(rotate_x + 90, 1.0, 0.0, 0.0);
				glRotatef(rotate_y, 0.0, 1.0, 0.0);
				glRotatef(rotate_z, 0.0, 0.0, 1.0);
				glScalef(camera_zoom_in + 0.5, camera_zoom_in + 0.5, camera_zoom_in + 0.5);
			}

			if (!view_trans)// 1인칭시점
			{
				glTranslatef(0.0, 0.0, 2000.0);
				viewing_trans(); // 1인칭시점을 만들어주는 함수 
			}
			Floor();
			Player();
			ITEM();
			Ai();

		}
		else if (state == 2)
		{
			glPushMatrix(); //ui
			{
				Ui();
			}
			glPopMatrix();

			if (view_trans) // 3인칭 시점
			{
				glTranslatef(eye_x, eye_y, eye_z);
				glRotatef(rotate_x + 90, 1.0, 0.0, 0.0);
				glRotatef(rotate_y, 0.0, 1.0, 0.0);
				glRotatef(rotate_z, 0.0, 0.0, 1.0);
				glScalef(camera_zoom_in + 0.5, camera_zoom_in + 0.5, camera_zoom_in + 0.5);
			}

			if (!view_trans)// 1인칭시점
			{
				glTranslatef(0.0, 0.0, 2000.0);
				viewing_trans(); // 1인칭시점을 만들어주는 함수 
			}
			Floor();
			Player();
			ITEM();
			Ai();
		}

	}
	glPopMatrix();


	glutSwapBuffers();

}
GLvoid Reshape(int w, int h)
{

	glMatrixMode(GL_PROJECTION);


	gluPerspective(60.0f, w / h, 1.0, 10000.0);
	glTranslatef(0.0, 0.0, -2000.0);
	glMatrixMode(GL_MODELVIEW);


	glViewport(0, 0, w, h);

}
bool collide_checking(float x1, float z1, float x2, float z2) // 충돌체크 함수
{
	if (x1 + 20 < x2 - 25)
		return false;
	if (x1 - 20 > x2 + 25)
		return false;
	if (z1 + 20 < z2 - 25)
		return false;
	if (z1 - 20 > z2 + 25)
		return false;
	return true;
}
bool collide_checking_item(float x1, float z1, float x2, float z2) // 충돌체크 함수
{
	if (x1 + 20 < x2 - 5)
		return false;
	if (x1 - 20 > x2 + 5)
		return false;
	if (z1 + 20 < z2 - 5)
		return false;
	if (z1 - 20 > z2 + 5)
		return false;
	return true;
}
bool collide_checking_me_and_enemy(float x1, float z1, float x2, float z2) // 충돌체크 함수
{
	if (x1 + 20 < x2 - 20)
		return false;
	if (x1 - 20 > x2 + 20)
		return false;
	if (z1 + 20 < z2 - 20)
		return false;
	if (z1 - 20 > z2 + 20)
		return false;
	return true;
}
void animate_enemy()
{
	if (state == 1)
	{
		for (int i = 0; i < 4; i++)
		{
			if (enemy[i].dir == 0)
			{
				enemy[i].degree = 0;
				enemy[i].z += enemy[i].speed;
			}
			if (enemy[i].dir == 1)
			{
				enemy[i].degree = 180;
				enemy[i].z -= enemy[i].speed;
			}
			if (enemy[i].dir == 2)
			{
				enemy[i].degree = 90;
				enemy[i].x += enemy[i].speed;
			}
			if (enemy[i].dir == 3)
			{
				enemy[i].degree = -90;
				enemy[i].x -= enemy[i].speed;
			}
		}



		for (int i = 0; i < 20; i++)
		{
			for (int j = 0; j < 20; j++)
			{
				for (int k = 0; k < 4; k++)
				{
					if (collide_checking(enemy[k].x, enemy[k].z, wall[i][j].x, wall[i][j].z))
					{
						if (enemy[k].dir == 0)
						{
							enemy[k].z -= 10;
							enemy[k].dir = rand() % 3 + 1;
						}
						else if (enemy[k].dir == 1)
						{
							enemy[k].z += 10;
							enemy[k].dir = rand() % 4;
						}
						else if (enemy[k].dir == 2)
						{
							enemy[k].x -= 10;
							enemy[k].dir = rand() % 4;
						}
						else if (enemy[k].dir == 3)
						{
							enemy[k].x += 10;
							enemy[k].dir = rand() % 4;
						}

					}
				}
			}
		}


		for (int i = 0; i < 4; i++)
		{
			if (collide_checking_me_and_enemy(my_x, my_z, enemy[i].x, enemy[i].z))
			{
				my_life -= 1;
				init_all();
			}
		}
	}

	else if (state == 2)
	{
		for (int i = 0; i < 7; i++)
		{
			if (enemy2[i].dir == 0)
			{
				enemy2[i].degree = 0;
				enemy2[i].z += enemy2[i].speed;
			}
			if (enemy2[i].dir == 1)
			{
				enemy2[i].degree = 180;
				enemy2[i].z -= enemy2[i].speed;
			}
			if (enemy2[i].dir == 2)
			{
				enemy2[i].degree = 90;
				enemy2[i].x += enemy2[i].speed;
			}
			if (enemy2[i].dir == 3)
			{
				enemy2[i].degree = -90;
				enemy2[i].x -= enemy2[i].speed;
			}
		}



		for (int i = 0; i < 20; i++)
		{
			for (int j = 0; j < 20; j++)
			{
				for (int k = 0; k < 7; k++)
				{
					if (collide_checking(enemy2[k].x, enemy2[k].z, wall2[i][j].x, wall2[i][j].z))
					{
						if (enemy2[k].dir == 0)
						{
							enemy2[k].z -= 10;
							enemy2[k].dir = rand() % 3 + 1;
						}
						else if (enemy2[k].dir == 1)
						{
							enemy2[k].z += 10;
							enemy2[k].dir = rand() % 4;
						}
						else if (enemy2[k].dir == 2)
						{
							enemy2[k].x -= 10;
							enemy2[k].dir = rand() % 4;
						}
						else if (enemy2[k].dir == 3)
						{
							enemy2[k].x += 10;
							enemy2[k].dir = rand() % 4;
						}

					}
				}
			}
		}


		for (int i = 0; i < 7; i++)
		{
			if (collide_checking_me_and_enemy(my_x, my_z, enemy2[i].x, enemy2[i].z))
			{
				my_life -= 1;
				init_all();
			}
		}
	}

}
void animate_my()
{
	if (my_dir == 0) // 아래방향
	{
		my_z += my_speed;
		my_degree = 90;
	}
	if (my_dir == 1) // 위 방향
	{
		my_z -= my_speed;
		my_degree = -90;
	}
	if (my_dir == 2) // 오른쪽방향
	{
		my_x += my_speed;
		my_degree = -180;
	}
	if (my_dir == 3) // 왼쪽방향
	{
		my_x -= my_speed;
		my_degree = 0;
	}
	if (state == 1)
	{
		for (int i = 0; i < 20; i++)
		{
			for (int j = 0; j < 20; j++)
			{
				if (collide_checking(my_x, my_z, wall[i][j].x, wall[i][j].z))
				{
					if (my_dir == 0)
					{
						my_dir = 4;
						my_z = my_z - 10;

					}
					else if (my_dir == 1)
					{
						my_dir = 4;
						my_z = my_z + 10;
					}
					else if (my_dir == 2)
					{
						my_dir = 4;
						my_x = my_x - 10;

					}
					else if (my_dir == 3)
					{
						my_dir = 4;
						my_x = my_x + 10;
					}
				}
			}
		}

		for (int i = 0; i < 20; i++)
		{
			for (int j = 0; j < 20; j++)
			{
				if (collide_checking_item(my_x, my_z, seed[i][j].x, seed[i][j].z))
				{
					seed[i][j].collide_variable = false;
					seed[i][j].x = 5000;
					seed[i][j].z = 5000;
					eat_count++;
					nowScore++;
				}
			}
		}
	}

	else if (state == 2)
	{
		for (int i = 0; i < 20; i++)
		{
			for (int j = 0; j < 20; j++)
			{
				if (collide_checking(my_x, my_z, wall2[i][j].x, wall2[i][j].z))
				{
					if (my_dir == 0)
					{
						my_dir = 4;
						my_z = my_z - 10;

					}
					else if (my_dir == 1)
					{
						my_dir = 4;
						my_z = my_z + 10;
					}
					else if (my_dir == 2)
					{
						my_dir = 4;
						my_x = my_x - 10;

					}
					else if (my_dir == 3)
					{
						my_dir = 4;
						my_x = my_x + 10;
					}
				}
			}
		}

		for (int i = 0; i < 20; i++)
		{
			for (int j = 0; j < 20; j++)
			{
				if (collide_checking_item(my_x, my_z, seed2[i][j].x, seed2[i][j].z))
				{
					seed2[i][j].collide_variable = false;
					seed2[i][j].x = 5000;
					seed2[i][j].z = 5000;
					eat_count2++;
					nowScore++;
					printf("%d \n", eat_count2);  //총갯수 212개
				}
			}
		}
	}

}
void TimerFunc(int value)
{
	if (state != 0)
	{
		timeCount++;
	}

	if (eat_count == 191 && state == 1) // 모든 먹이를 먹을시 스테이지2로 진입. //191
	{
		state = 2;
		init_all();
		init_sound();
	}

	if (my_life == 0 || (timeCount / 10) == 600) //////////////플레이어 목숨이 0일때 처음으로돌아감
	{
		init_all();
		isGameOver = true;
		my_life = 3;
		eat_count = 0;
		eat_count2 = 0;
	}

	if (eat_count2 == 212 && state == 2) //총 212개
	{
		state = 0;
		init_all();
	}

	if (isGameOver)
	{
		state = 4;
		init_sound();
	}
	animate_my();
	animate_enemy();
	glutPostRedisplay();
	glutTimerFunc(100, TimerFunc, 1);

}
void print(int x, int y, int z, char *string)
{
	glRasterPos2f(x, y);
	int len = (int)strlen(string);
	for (int i = 0; i < len; i++)
	{
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, string[i]);
	}
};
void GameIntro()
{
	glEnable(GL_TEXTURE_2D);
	glPushMatrix();
	{
		glScalef(introCameraZoomin, introCameraZoomin, introCameraZoomin);
		glPushMatrix();
		{
			char string[] = "Pac-Man 3D";
			print(-36, 90, 0, string);
		}
		glPopMatrix();

		glPushMatrix();
		{

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glBindTexture(GL_TEXTURE_2D, textures[0]);

			glBegin(GL_QUADS);
			{
				glColor3f(1.0, 1.0, 1.0);
				glTexCoord2f(0.0f, 0.0f);
				glVertex3f(-500, -500, -450);
				glTexCoord2f(1.0f, 0.0f);
				glVertex3f(500, -500, -450);
				glTexCoord2f(1.0f, 1.0f);
				glVertex3f(500, 500, -450);
				glTexCoord2f(0.0f, 1.0f);
				glVertex3f(-500, 500, -450);

			}
			glEnd();
		}
		glPopMatrix();

		glPushMatrix();
		{
			char string2[] = "Press SpaceBar to Start";
			print(-65, -100, 0, string2);
		}
		glPopMatrix();


	}
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
}
void GameOver()
{
	tries++;
	glPushMatrix();
	{
		glScalef(introCameraZoomin, introCameraZoomin, introCameraZoomin);

		glPushMatrix();
		{
			char string[] = "High Score : ";
			glColor3f(1, 0, 0);
			print(-80, 100, 0, string);

			glColor3f(1, 1, 0);
			print(40, 100, 0, itoa(displayHigh, buffer, 10));
		}
		glPopMatrix();

		glPushMatrix();
		{
			char string[] = "Your Score : ";
			glColor3f(1, 0, 0);
			print(-80, 50, 0, string);

			glColor3f(1, 1, 0);
			print(40, 50, 0, itoa(LastScore, buffer, 10));
		}
		glPopMatrix();

		if (displayHigh == LastScore)
		{
			glPushMatrix();
			{
				char string[] = "Congratulations! Top-Score!";
				glColor3f(1, 0, 0);
				print(-90, 10, 0, string);
			}
			glPopMatrix();


		}

		glPushMatrix();
		{
			char string[] = "Press '+' to ReStart";
			glColor3f(1, 1, 0);
			print(-70, -60, 0, string);
		}
		glPopMatrix();

	}
	glPopMatrix();


	if (isGameOver)
	{
		if (Write_HighScore == 1)
			save_highscore(FormerHigh);

		my_life = 3;
		eat_count = 0;
		eat_count2 = 0;

		state = 1;

	}

}
void save_highscore(int new_score)
{
	FILE *highscore;

	highscore = fopen("highscore.txt", "w");

	if (highscore)
	{
		fprintf(highscore, "%d", new_score);
		fclose(highscore);
	}

	Write_HighScore = 0;
}
void initTextures()
{
	glGenTextures(6, textures);
	glBindTexture(GL_TEXTURE_2D, textures[0]);
	pBytes = LoadDIBitmap("testintro.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 318, 270, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
}
GLubyte * LoadDIBitmap(const char *filename, BITMAPINFO **info)
{
	FILE *fp;
	GLubyte *bits;
	int bitsize, infosize;
	BITMAPFILEHEADER header;
	// 바이너리 읽기 모드로 파일을 연다
	if ((fp = fopen(filename, "rb")) == NULL)
		return NULL;
	// 비트맵 파일 헤더를 읽는다.
	if (fread(&header, sizeof(BITMAPFILEHEADER), 1, fp) < 1)
	{
		fclose(fp);
		return NULL;
	}
	// 파일이 BMP 파일인지 확인한다.
	if (header.bfType != 'MB')
	{
		fclose(fp);
		return NULL;
	}
	// BITMAPINFOHEADER 위치로 간다.
	infosize = header.bfOffBits - sizeof(BITMAPFILEHEADER);
	// 비트맵 이미지 데이터를 넣을 메모리 할당을 한다.
	if ((*info = (BITMAPINFO *)malloc(infosize)) == NULL)
	{
		fclose(fp);
		exit(0);
		return NULL;
	}
	// 비트맵 인포 헤더를 읽는다.
	if (fread(*info, 1, infosize, fp) < (unsigned int)infosize)
	{
		free(*info);
		fclose(fp);
		return NULL;
	}
	// 비트맵의 크기 설정
	if ((bitsize = (*info)->bmiHeader.biSizeImage) == 0)
		bitsize = ((*info)->bmiHeader.biWidth * (*info)->bmiHeader.biBitCount + 7) / 8.0 * abs((*info)->bmiHeader.biHeight);
	// 비트맵의 크기만큼 메모리를 할당한다.
	if ((bits = (unsigned char *)malloc(bitsize)) == NULL)
	{
		free(*info);
		fclose(fp);
		return NULL;
	}
	// 비트맵 데이터를 bit(GLubyte 타입)에 저장한다.
	if (fread(bits, 1, bitsize, fp) < (unsigned int)bitsize)
	{
		free(*info); free(bits);
		fclose(fp);
		return NULL;
	}
	fclose(fp);
	return bits;
}