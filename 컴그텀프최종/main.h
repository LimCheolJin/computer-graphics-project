#pragma once
#pragma once
# define PI 3.141592
#include <gl/freeglut.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>
#include <windows.h>
#include <time.h>
#include <iostream>
#include <mmsystem.h>
#include <fstream>
#pragma warning(disable: 4996)
#pragma comment(lib,"winmm.lib") 
using namespace std;

GLvoid drawScene(GLvoid);
GLvoid Reshape(int w, int h);
void keyboard(unsigned char, int, int);
void TimerFunc(int);
void SpecialKeyboard(int, int, int);
void viewing_trans(void);
void GameIntro();
bool collide_checking(float x1, float z1, float x2, float z2);
bool collide_checking_item(float x1, float z1, float x2, float z2);
bool collide_checking_me_and_enemy(float x1, float z1, float x2, float z2);
void print(int x, int y, int z, char *string);
void Ai();
void Player();
void Ui();
void init_all();
void animate_enemy();
void animate_my();
void init_sound();

typedef struct Item
{
	float x, y, z = 0;
	bool collide_variable = true;
}Item;

typedef struct Enemy
{
	float x, y, z = 0.0;
	int dir = 0;
	int speed = 10;
	float degree = 0.0;
}Enemy;

typedef struct Wall
{
	float x, y, z = 0.0;
	float color_r = 0.0;
	float color_g = 0.0;
	float color_b = 0.0;
}Wall;

////STAGE1
Wall wall[20][20] = { 0 };
Item seed[20][20] = { 0 };
Enemy enemy[4] = { 0 };
////STAGE2
Wall wall2[20][20] = { 0 };
Item seed2[20][20] = { 0 };
Enemy enemy2[7] = { 0 };

///////////////////////////////////////////
int temp[20][20];
int temp2[20][20];
///////////////////////////////////////////
//카메라
float eye_x, eye_y, eye_z; // 임시 카메라 이동 변수
float camera_zoom_in = 1.0, camera_zoom_out = 1.0; // 임시 카메라 확대 축소 변수
float rotate_x, rotate_y, rotate_z; // 임시 카메라 회전 변수
									////////////////////////////////////////////

									////////////////////////////////////////////
									//플레이어									
float my_x = -175.0, my_y = 50, my_z = 325.0; // 나의 이동변수
int my_dir = 2; //나의 방향, 4로 한이유는 4에는 아무것도 없음 0,1,2,3 에는 방향이들어있음
float my_degree = 0.0; // 나의 회전 각
int my_speed = 10;
int my_life = 3;
int FormerHigh = 0;
int eat_count = 0;
int eat_count2 = 0;
////////////////////////////////////////////

int state = 0; // 0 = intro // 1 = stage1 // 2 =stage2 // 3 = gameover
float introCameraZoomin = 5;

bool view_trans = false; // 시점 변환 (1인칭,3인칭 을위한 불변수) true일땐 3인칭 false일땐 1인칭
						 ////////////////////////////////////////
GLfloat diffuse_quantity = 1.0, specular_quantity = 1.0, ambient_quantity = 1.0;
GLfloat AmbientLight[] = { 1.0f*ambient_quantity  , 1.0f*ambient_quantity  , 1.0f*ambient_quantity , 1.0f };
GLfloat DiffuseLight[] = { 1.0 * diffuse_quantity, 1.0f * diffuse_quantity, 1.0f * diffuse_quantity, 1.0f };
GLfloat SpecularLight[] = { 1.0 * specular_quantity, 1.0 * specular_quantity, 1.0 * specular_quantity, 1.0f };
GLfloat lightPos1[] = { -700, 400,0,1.0 };
GLfloat lightPos2[] = { 700, 400, 0,1.0 };
bool light_1_state = true;
bool light_2_state = true;
bool diffuse_quantity_select = true;
bool specular_quantity_select = true;
bool ambient_quantity_select = true;
////////////////////////////////////////
bool isGameOver = false;
void GameOver();
char buffer[65] = { 0 };
int nowScore;
int LastScore;
int tries;
int Write_HighScore;
int displayHigh;
bool gameover_sound = true;
void save_highscore(int);
////////////////////////////////////////
void initTextures();
GLubyte * LoadDIBitmap(const char *filename, BITMAPINFO **info);
GLubyte *pBytes; // 데이터를 가리킬 포인터
BITMAPINFO *info; // 비트맵 헤더 저장할 변수
GLuint textures[5];
////////////////////////////////////////
